#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import re
import json


def main():
    import argparse
    parser = argparse.ArgumentParser(description='TODO')
    parser.add_argument('-r', '--results', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='full_results.txt')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-c', '--config', type=str,
                        required=True,
                        help='Json config')
    args = parser.parse_args()

    values = '\s*(\d+)\s*\|\s*([0-9.]+)%\s*'
    match_re = re.compile(f'^Match:{values}$')
    part_match_re = re.compile(f'^Partial match:{values}$')
    unmatch_re = re.compile(f'^Unmatch:{values}$')
    for line in args.results:
        if match_re.match(line):
            # match_num = int(match_re.match(line).group(1))
            match_per = float(match_re.match(line).group(2))
        if part_match_re.match(line):
            # part_match_num = int(part_match_re.match(line).group(1))
            part_match_per = float(part_match_re.match(line).group(2))
        if unmatch_re.match(line):
            # unmatch_num = int(unmatch_re.match(line).group(1))
            unmatch_per = float(unmatch_re.match(line).group(2))


    with open(args.config, 'r') as f:
        config_data = json.load(f)

    learning_rate = config_data['learning_rate']
    batch_size = config_data['batch_size']
    gradient_accumulation_steps = config_data['gradient_accumulation_steps']
    # num_train_epochs = config_data['num_train_epochs']
    weight_decay = config_data['weight_decay']
    train_data = config_data['train_data']

    args.output.write(f'{train_data} & {gradient_accumulation_steps} & {batch_size} & {learning_rate} & {weight_decay} & {match_per:.2f} & {part_match_per:.2f} & {unmatch_per:.2f} \\\\ % {args.config}\n')


if __name__ == '__main__':
    main()
