data_v3:
	mkdir $@
	./make_train_data.py --out $@ --url '0.0.0.0' --port 9001 -p /nlp/projekty/sqad/sqad_v3_answer_selection/60_30_10 2> $@.log

data_q_type_v3:
	mkdir $@
	./make_train_data.py --out $@ --url '0.0.0.0' --port 9001 -p /nlp/projekty/sqad/sqad_v3_answer_selection/60_30_10 --add_q_type 2> $@.log

data_q_type_v3.2:
	mkdir $@
	./make_train_data.py --out $@ --db_path /nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database_devel/sqad_db/devel/v3_2_16-09-2022_16-34-35/v3_2_16-09-2022_16-34-35_base -s parts_v3.2.dump --add_q_type 2> $@.log

finetunning:
	#python3 ./fine_tunning.py --url '0.0.0.0' --port 9001
	for i in `seq 1 5`; do\
		echo "combination $$i"; \
  		python3 ./generate_config.py -c $$i -o config_train.py;\
		python3 ./fine_tunning.py --url '0.0.0.0' --port 9001 --notes "c=$$i";\
	done
	echo "AQA Answer Extraction fine tuning done" | mail -s "AQA AE" marek.medved3@gmail.com

finetunning_q_type:
	#python3 ./fine_tunning.py --url '0.0.0.0' --port 9001 --notes "q_type"
	for i in `seq 1 50`; do\
		echo "combination $$i"; \
  		python3 ./generate_config.py -c $$i -d "./data_q_type/" -o config_train.py;\
		python3 ./fine_tunning.py --url '0.0.0.0' --port 9001 --notes "q_type;c=$$i";\
	done
	echo "AQA Answer Extraction with question fine tuning done" | mail -s "AQA AE Q" marek.medved3@gmail.com

extraction_file:
	./answer_extraction.py -m train_2022_08_12_18-59-18/xlm-roberta-large-squad2-finetuned-V1 --input_file data/test --eval_with_db --url '0.0.0.0' --port 9001 -o ./test_2022_08_12_18-59-18/

extraction_stdin:
	./answer_extraction.py -i "kdo je tom hanks ?|thomas jeffrey \" tom \" hanks ( * 9 . července 1956 concord ) je americký filmový herec , režisér a producent , držitel dvou oscarů za herecký výkon ." -m train_2022_08_12_18-59-18/xlm-roberta-large-squad2-finetuned-V1

clean:
	rm -rf data/*

run_service:
	./ml_answer_extraction_server.py --port 9003 --model_path ./train_2022_08_16_17-19-20/xlm-roberta-large-squad2-finetuned-V1/

table:
	@echo "train_data\tgradient_accumulation_steps\tbatch_size\tlearning_rate\tweight_decay\tmatch_per\tpart_match_per\tunmatch_per"
	@for i in `find ./ -name "train_2022*"`; do \
		cat $$i/test_results/full_results.txt | ./stats2table.py -c $$i/xlm-roberta-large-squad2-finetuned-V1.json; \
	done

