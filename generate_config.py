#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import itertools


def i(num):
    return ' ' * (4 * num)


def get_save_config(out):
    out.write(f'{i(1)}def save_config(self, file_name: str) -> None:\n')
    out.write(f'{i(2)}config_dict = {{}}\n')
    out.write(f'{i(2)}for key, value in vars(self).items():\n')
    out.write(f'{i(3)}if key.startswith("_") or isinstance(value, staticmethod):\n')
    out.write(f'{i(4)}continue\n')
    out.write(f'{i(3)}config_dict[key] = value\n')
    out.write(f'{i(2)}with open(file_name, "w") as out_file:\n')
    out.write(f'{i(3)}json.dump(config_dict, out_file, indent=2, sort_keys=False)\n')


def compute_all_combinations(params):
    keys, values = zip(*params.items())
    return [dict(zip(keys, v)) for v in itertools.product(*values)]


def generate_config(args, params):
    sys.stdout.write(f'params: {params}\n')
    # https://huggingface.co/docs/transformers/v4.21.1/en/main_classes/trainer#transformers.TrainingArguments

    args.output.write(f'import sys\n')
    args.output.write(f'import json\n')
    args.output.write(f'import time\n')
    args.output.write(f'class Config:\n')
    args.output.write(f'{i(1)}def __init__(self):\n')
    args.output.write(f'{i(2)}# Path\n')
    args.output.write(f'{i(2)}self.train_data = "{params["data"]}"\n')
    args.output.write(f'{i(2)}self.model_path = "deepset/xlm-roberta-large-squad2"\n')
    args.output.write(f'{i(2)}self.output_dir = "./train_" + time.strftime("%Y_%m_%d_%H-%M-%S")\n')
    args.output.write(f'{i(2)}# Base\n')
    args.output.write(f'{i(2)}self.model_name = "xlm-roberta-large-squad2"\n')
    args.output.write(f'{i(2)}self.version = "V1"\n')
    args.output.write(f'{i(2)}self.debug = False\n')
    args.output.write(f'{i(2)}self.strategy = "epoch"\n')
    args.output.write(f'{i(2)}self.load_best_model_at_end = "True"\n')
    args.output.write(f'{i(2)}# Tokenizer\n')
    args.output.write(f'{i(2)}self.doc_stride = 128\n')
    args.output.write(f'{i(2)}self.max_length = 256\n')
    args.output.write(f'{i(2)}# Trainer\n')
    args.output.write(f'{i(2)}self.batch_size = {params["batch_size"]}\n')
    args.output.write(f'{i(2)}if self.batch_size >= 64:\n')
    args.output.write(f'{i(3)}sys.stderr.write("Did you choose A100? export CUDA_VISIBLE_DEVICES=0\\n")\n')
    args.output.write(f'{i(2)}self.optimizer = "{params["optimizer"]}"\n')
    args.output.write(f'{i(2)}self.learning_rate = {params["learning_rate"]}\n')
    args.output.write(f'{i(2)}self.warmup_ratio = {params["warmup_ratio"]}\n')
    args.output.write(f'{i(2)}self.gradient_accumulation_steps = {params["gradient_accumulation_steps"]}\n')
    args.output.write(f'{i(2)}self.num_train_epochs = {params["num_train_epochs"]}\n')
    args.output.write(f'{i(2)}self.weight_decay = {params["weight_decay"]}\n')
    args.output.write(f'{i(2)}# Postprocess\n')
    args.output.write(f'{i(2)}self.n_best_size = 20\n')
    args.output.write(f'{i(2)}self.max_answer_length = 256\n')
    args.output.write(f'{i(2)}# Notes\n')
    args.output.write(f'{i(2)}self.scores = {{}}\n')
    args.output.write(f'{i(2)}self.notes = ""\n')

    args.output.write('\n')
    # Fixed part
    get_save_config(args.output)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='generate config_train.py')
    parser.add_argument('-c', '--combination', type=int,
                        required=True,
                        help='Combination of parameters index')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    args = parser.parse_args()

    params = {
        'batch_size': [64, 96],
        'optimizer': ['adamw_torch', 'adamw_hf', 'adamw_apex_fused'],
        'learning_rate': [0.0001, 0.00001],
        'warmup_ratio': [0.1, 0.01],
        'gradient_accumulation_steps': [4, 8],
        'num_train_epochs': [30],
        'weight_decay': [0.0001, 0.001],
        'data': ['./data/', './data_q_type/']
    }
    param_combination = [

    {'batch_size': 64, 'optimizer': 'adamw_torch', 'learning_rate': 0.000001, 'warmup_ratio': 0.01, 'gradient_accumulation_steps': 6, 'num_train_epochs': 30, 'weight_decay': 0.01, 'data': './data_q_type/'},
    {'batch_size': 96, 'optimizer': 'adamw_torch', 'learning_rate': 0.000001, 'warmup_ratio': 0.01, 'gradient_accumulation_steps': 6, 'num_train_epochs': 30, 'weight_decay': 0.01, 'data': './data/'},
    {'batch_size': 64, 'optimizer': 'adamw_hf', 'learning_rate': 0.001, 'warmup_ratio': 0.001, 'gradient_accumulation_steps': 4, 'num_train_epochs': 30, 'weight_decay': 0.01, 'data': './data/'},
    {'batch_size': 96, 'optimizer': 'adamw_hf', 'learning_rate': 0.001, 'warmup_ratio': 0.001, 'gradient_accumulation_steps': 4, 'num_train_epochs': 30, 'weight_decay': 0.01, 'data': './data_q_type/'},
                        ]

    # param_combination = compute_all_combinations(params)
    generate_config(args, param_combination[args.combination-1])


if __name__ == '__main__':
    main()
