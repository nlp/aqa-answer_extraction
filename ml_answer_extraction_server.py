#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved

import json
import os
import logging
from transformers import pipeline
from transformers import AutoTokenizer
from transformers import AutoModelForQuestionAnswering
from http.server import HTTPServer, BaseHTTPRequestHandler
current_dir = os.path.dirname(os.path.realpath(__file__))

answer_extraction_pipeline = None


class JobServer(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _get_data(self):
        content_length = int(self.headers['Content-Length'])
        return json.loads(self.rfile.read(content_length))

    def do_POST(self):
        command = self.path[1:]
        data = self._get_data()

        qa_input = {'question': data['question'],
                    'context': data['answer_selection']}

        prediction = answer_extraction_pipeline(qa_input)

        logging.info(f'POST {command}; {data["question"]}, {data["answer_selection"]}')
        self._set_headers()
        self.wfile.write(json.dumps({'command': command,
                                     'question': data['question'],
                                     'answer_selection': data["answer_selection"],
                                     'answer_extraction': prediction["answer"],
                                     'answer_extraction_score': prediction["score"]}).encode('utf-8'))


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Fasttext word embedding service')
    parser.add_argument('--port', type=int,
                        required=True, default=9003,
                        help='Port')
    parser.add_argument("--model_path",
                        help="Specifies model path", required=True)
    args = parser.parse_args()
    global answer_extraction_pipeline
    tokenizer = AutoTokenizer.from_pretrained(args.model_path)
    tokenizer.padding_side = "right"
    model = AutoModelForQuestionAnswering.from_pretrained(args.model_path)
    answer_extraction_pipeline = pipeline('question-answering', model=model, tokenizer=tokenizer)

    logging.basicConfig(filename=f'{current_dir}/server.log',
                        filemode='a',
                        format='%(asctime)s|%(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.DEBUG)

    logging.info(f'Starting httpd on port {args.port}...')
    httpd = HTTPServer(('localhost', args.port), JobServer)
    sys.stderr.write(f'Running Answer extraction service (AE-ML) on port {args.port}\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == "__main__":
    main()