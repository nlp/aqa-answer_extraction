#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import os
import json
from sqad_db import SqadDb
from query_database import get_record
from transformers import pipeline
from transformers import AutoTokenizer
from transformers import AutoModelForQuestionAnswering


def get_record_parts(rid, db):
    """
    Get sentence representation form database
    :param rid: str, record id
    :param db: sqad_db object, connection to database
    :return: str, str, str
    """
    vocabulary, _, kb = db.get_dicts()
    record = get_record(db, rid, word_parts='w')

    question_content = ''
    for question_sent in record['question']:
        for token in question_sent['sent']:
            question_content += f'{token["word"]} '

    correct_answer_content = ''
    for answer_selection_sent in record['a_sel']:
        for token in answer_selection_sent['sent']:
            correct_answer_content += f'{token["word"]} '

    answer_extraction_content = ''
    for answer_extraction_sent in record['a_ext']:
        for token in answer_extraction_sent['sent']:
            answer_extraction_content += f'{token["word"]} '

    return question_content, correct_answer_content, answer_extraction_content, record['q_type'], record['a_type']


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Answer extraction')
    parser.add_argument('-i', '--input', type=str,
                        required=False, default='',
                        help='Input string: "Question | Context"')
    # parser.add_argument('-f', '--input_file', type=argparse.FileType('r'),
    #                     required=False, default='',
    #                     help='Input json')
    parser.add_argument('-o', '--output', type=str,
                        required=False,
                        help='Output directory')
    parser.add_argument('--eval_with_db', action='store_true',
                        required=False, default=False,
                        help='Evaluate results according to SQAD')
    parser.add_argument('-m', '--model_path', type=str,
                        required=True,
                        help='Pretrained model')
    parser.add_argument('--db_path', type=str,
                        required=False,
                        help='Database path')
    parser.add_argument('--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('--port', type=int,
                        required=False, default=None,
                        help='Server port')
    args = parser.parse_args()

    match = 0
    partial_match = 0
    un_match = 0
    total_num = 0

    # sys.stderr.write('Implement remove bracket content form answer extraction!!!!')
    # sys.exit()

    # ===================================================
    # Input load
    # print(args.input)
    # if args.input_file:
    #     with open(args.input_file, 'rb') as f:
    #         czech_dataset = json.load(f)['data']
    # el
    if args.input:
        question, context = args.input.strip().split('|')
        czech_dataset = [{'id': 0, 'question': question, 'context': context}]
    else:
        sys.stderr.write('No input provided!')
        sys.exit(2)

    # ===================================================
    # DB for results load
    if args.eval_with_db:
        if not args.output:
            sys.stderr.write('Have to specify output directory!')
            sys.exit(2)

        if not os.path.exists(f'{args.output}/test_results/'):
            os.makedirs(f'{args.output}/test_results/')

        db = None
        if (args.url and args.port) or args.db_path:
            if args.url and args.port:
                db = SqadDb(url=args.url, port=args.port, read_only=True)
            elif args.db_path:
                db = SqadDb(file_name=args.db_path, read_only=True)
        else:
            sys.stderr.write('Please specify --db_path or (--port and --url)')
            sys.exit()

    # ===================================================
    # Model load
    tokenizer = AutoTokenizer.from_pretrained(args.model_path)
    tokenizer.padding_side = "right"
    model = AutoModelForQuestionAnswering.from_pretrained(args.model_path)
    nlp = pipeline('question-answering', model=model, tokenizer=tokenizer)

    for example in czech_dataset:
        rec_id = example['id']

        QA_input = {
            'question': example['question'],
            'context': example['context']
        }
        prediction = nlp(QA_input)

        if args.eval_with_db:
            total_num += 1
            question, answer_sentence, orig_answer_extraction, q_type, a_type = get_record_parts(rec_id, db)
            with open(f'{args.output}/test_results/{rec_id}', 'w') as f:

                f.write(f'Record: {rec_id}\n')
                f.write(f'Question: {question}\n')
                f.write(f'Type: q:{q_type} a:{a_type}\n')
                f.write(f'Answer selection: {answer_sentence}\n')
                f.write(f'Original extraction: {orig_answer_extraction}\n')
                f.write(f'Prediction: {prediction["answer"]}\n')
                f.write(f'Prediction score: {prediction["score"]}\n')

                lower_pred = prediction["answer"].lower()
                lower_orig = orig_answer_extraction.lower()
                if lower_orig in lower_pred:
                    match += 1
                    f.write(f'Match\n')
                elif len(set(lower_orig.split(' ')) & set(lower_pred.split(' '))) > 0:
                    partial_match += 1
                    f.write(f'Partial match\n')
                else:
                    un_match += 1
                    f.write(f'Unmatch\n')
        else:
            print(prediction["answer"])

    if args.eval_with_db:
        with open(f'{args.output}/test_results/full_results.txt', 'w') as f:
            f.write(f'Model: {args.model_path}\n')
            f.write(f'Evaluated on: {args.input_file}\n')
            f.write('Final results:\n')
            f.write(f'Match: {match} | {(match * 100) / total_num}%\n')
            f.write(f'Partial match: {partial_match} | {(partial_match * 100) / total_num}%\n')
            f.write(f'Unmatch: {un_match} | {(un_match * 100) / total_num}%\n')


if __name__ == '__main__':
    main()
