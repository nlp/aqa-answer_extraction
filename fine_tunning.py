#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
# source: https://www.kaggle.com/code/bhavikardeshna/fine-tuning-xlm-roberta-question-answering/notebook
# model: https://huggingface.co/deepset/xlm-roberta-large-squad2

import os
import sys
import datasets
import logging
import collections
import numpy as np
from typing import Tuple
from tqdm.auto import tqdm
from sqad_db import SqadDb
from query_database import get_record
from config_train import Config
from transformers import AutoTokenizer
from transformers import PreTrainedTokenizerFast
from transformers import AutoModelForQuestionAnswering
from transformers import TrainingArguments
from transformers import Trainer
from transformers import default_data_collator

conf = Config()


def prepare_train_features(examples, tokenizer, pad_on_right):
    # Some of the questions have lots of whitespace on the left, which is not useful and will make the
    # truncation of the context fail (the tokenized question will take a lots of space). So we remove that
    # left whitespace
    examples["question"] = [q.lstrip() for q in examples["question"]]

    # Tokenize our examples with truncation and padding, but keep the overflows using a stride. This results
    # in one example possible giving several features when a context is long, each of those features having a
    # context that overlaps a bit the context of the previous feature.
    tokenized_examples = tokenizer(examples["question" if pad_on_right else "context"],
                                   examples["context" if pad_on_right else "question"],
                                   truncation="only_second" if pad_on_right else "only_first",
                                   max_length=conf.max_length,
                                   stride=conf.doc_stride,
                                   return_overflowing_tokens=True,
                                   return_offsets_mapping=True,
                                   padding="max_length")

    # Since one example might give us several features if it has a long context, we need a map from a feature to
    # its corresponding example. This key gives us just that.
    sample_mapping = tokenized_examples.pop("overflow_to_sample_mapping")
    # The offset mappings will give us a map from token to character position in the original context. This will
    # help us compute the start_positions and end_positions.
    offset_mapping = tokenized_examples.pop("offset_mapping")

    # Let's label those examples!
    tokenized_examples["start_positions"] = []
    tokenized_examples["end_positions"] = []

    for i, offsets in enumerate(offset_mapping):
        # We will label impossible answers with the index of the CLS token.
        input_ids = tokenized_examples["input_ids"][i]
        cls_index = input_ids.index(tokenizer.cls_token_id)

        # Grab the sequence corresponding to that example (to know what is the context and what is the question).
        sequence_ids = tokenized_examples.sequence_ids(i)

        # One example can give several spans, this is the index of the example containing this span of text.
        sample_index = sample_mapping[i]
        answers = examples["answers"][sample_index]
        # If no answers are given, set the cls_index as answer.
        if len(answers["answer_start"]) == 0:
            tokenized_examples["start_positions"].append(cls_index)
            tokenized_examples["end_positions"].append(cls_index)
        else:
            # Start/end character index of the answer in the text.
            start_char = answers["answer_start"][0]
            end_char = start_char + len(answers["text"][0])

            # Start token index of the current span in the text.
            token_start_index = 0
            while sequence_ids[token_start_index] != (1 if pad_on_right else 0):
                token_start_index += 1

            # End token index of the current span in the text.
            token_end_index = len(input_ids) - 1
            while sequence_ids[token_end_index] != (1 if pad_on_right else 0):
                token_end_index -= 1

            # Detect if the answer is out of the span (in which case this feature is labeled with the CLS index).
            if not (offsets[token_start_index][0] <= start_char and offsets[token_end_index][1] >= end_char):
                tokenized_examples["start_positions"].append(cls_index)
                tokenized_examples["end_positions"].append(cls_index)
            else:
                # Otherwise move the token_start_index and token_end_index to the two ends of the answer.
                # Note: we could go after the last offset if the answer is the last word (edge case).
                while token_start_index < len(offsets) and offsets[token_start_index][0] <= start_char:
                    token_start_index += 1
                tokenized_examples["start_positions"].append(token_start_index - 1)
                while offsets[token_end_index][1] >= end_char:
                    token_end_index -= 1
                tokenized_examples["end_positions"].append(token_end_index + 1)

    return tokenized_examples


def prepare_validation_features(examples, tokenizer, pad_on_right):
    # Some of the questions have lots of whitespace on the left, which is not useful and will make the
    # truncation of the context fail (the tokenized question will take a lots of space). So we remove that
    # left whitespace
    examples["question"] = [q.lstrip() for q in examples["question"]]

    # Tokenize our examples with truncation and maybe padding, but keep the overflows using a stride. This results
    # in one example possible giving several features when a context is long, each of those features having a
    # context that overlaps a bit the context of the previous feature.
    tokenized_examples = tokenizer(
        examples["question" if pad_on_right else "context"],
        examples["context" if pad_on_right else "question"],
        truncation="only_second" if pad_on_right else "only_first",
        max_length=conf.max_length,
        stride=conf.doc_stride,
        return_overflowing_tokens=True,
        return_offsets_mapping=True,
        padding="max_length",
    )

    # Since one example might give us several features if it has a long context, we need a map from a feature to
    # its corresponding example. This key gives us just that.
    sample_mapping = tokenized_examples.pop("overflow_to_sample_mapping")

    # We keep the example_id that gave us this feature and we will store the offset mappings.
    tokenized_examples["example_id"] = []

    for i in range(len(tokenized_examples["input_ids"])):
        # Grab the sequence corresponding to that example (to know what is the context and what is the question).
        sequence_ids = tokenized_examples.sequence_ids(i)
        context_index = 1 if pad_on_right else 0

        # One example can give several spans, this is the index of the example containing this span of text.
        sample_index = sample_mapping[i]
        tokenized_examples["example_id"].append(examples["id"][sample_index])

        # Set to None the offset_mapping that are not part of the context so it's easy to determine if a token
        # position is part of the context or not.
        tokenized_examples["offset_mapping"][i] = [
            (o if sequence_ids[k] == context_index else None)
            for k, o in enumerate(tokenized_examples["offset_mapping"][i])
        ]

    return tokenized_examples


def postprocess_qa_predictions(
        examples,
        features,
        predictions: Tuple[np.ndarray, np.ndarray],
        version_2_with_negative: bool = False,
        n_best_size: int = conf.n_best_size,
        max_answer_length: int = conf.max_answer_length,
        null_score_diff_threshold: float = 0.0,
):
    """
    Post-processes the predictions of a question-answering model to convert them to answers that are substrings of the
    original contexts. This is the base postprocessing functions for models that only return start and end logits.
    Args:
        examples: The non-preprocessed dataset (see the main script for more information).
        features: The processed dataset (see the main script for more information).
        predictions (:obj:`Tuple[np.ndarray, np.ndarray]`):
            The predictions of the model: two arrays containing the start logits and the end logits respectively. Its
            first dimension must match the number of elements of :obj:`features`.
        version_2_with_negative (:obj:`bool`, `optional`, defaults to :obj:`False`):
            Whether or not the underlying dataset contains examples with no answers.
        n_best_size (:obj:`int`, `optional`, defaults to 20):
            The total number of n-best predictions to generate when looking for an answer.
        max_answer_length (:obj:`int`, `optional`, defaults to 30):
            The maximum length of an answer that can be generated. This is needed because the start and end predictions
            are not conditioned on one another.
        null_score_diff_threshold (:obj:`float`, `optional`, defaults to 0):
            The threshold used to select the null answer: if the best answer has a score that is less than the score of
            the null answer minus this threshold, the null answer is selected for this example (note that the score of
            the null answer for an example giving several features is the minimum of the scores for the null answer on
            each feature: all features must be aligned on the fact they `want` to predict a null answer).
            Only useful when :obj:`version_2_with_negative` is :obj:`True`.
    """
    assert len(predictions) == 2, "`predictions` should be a tuple with two elements (start_logits, end_logits)."
    all_start_logits, all_end_logits = predictions

    assert len(predictions[0]) == len(features), f"Got {len(predictions[0])} predictions and {len(features)} features."

    # Build a map example to its corresponding features.
    example_id_to_index = {k: i for i, k in enumerate(examples["id"])}
    features_per_example = collections.defaultdict(list)
    for i, feature in enumerate(features):
        features_per_example[example_id_to_index[feature["example_id"]]].append(i)

    # The dictionaries we have to fill.
    all_predictions = collections.OrderedDict()
    all_nbest_json = collections.OrderedDict()
    if version_2_with_negative:
        scores_diff_json = collections.OrderedDict()


    # Let's loop over all the examples!
    for example_index, example in enumerate(tqdm(examples)):
        # Those are the indices of the features associated to the current example.
        feature_indices = features_per_example[example_index]

        min_null_prediction = None
        prelim_predictions = []

        # Looping through all the features associated to the current example.
        for feature_index in feature_indices:
            # We grab the predictions of the model for this feature.
            start_logits = all_start_logits[feature_index]
            end_logits = all_end_logits[feature_index]
            # This is what will allow us to map some the positions in our logits to span of texts in the original
            # context.
            offset_mapping = features[feature_index]["offset_mapping"]
            # Optional `token_is_max_context`, if provided we will remove answers that do not have the maximum context
            # available in the current feature.
            token_is_max_context = features[feature_index].get("token_is_max_context", None)

            # Update minimum null prediction.
            feature_null_score = start_logits[0] + end_logits[0]
            if min_null_prediction is None or min_null_prediction["score"] > feature_null_score:
                min_null_prediction = {
                    "offsets": (0, 0),
                    "score": feature_null_score,
                    "start_logit": start_logits[0],
                    "end_logit": end_logits[0],
                }

            # Go through all possibilities for the `n_best_size` greater start and end logits.
            start_indexes = np.argsort(start_logits)[-1: -n_best_size - 1: -1].tolist()
            end_indexes = np.argsort(end_logits)[-1: -n_best_size - 1: -1].tolist()
            for start_index in start_indexes:
                for end_index in end_indexes:
                    # Don't consider out-of-scope answers, either because the indices are out of bounds or correspond
                    # to part of the input_ids that are not in the context.
                    if (
                            start_index >= len(offset_mapping)
                            or end_index >= len(offset_mapping)
                            or offset_mapping[start_index] is None
                            or offset_mapping[end_index] is None
                    ):
                        continue
                    # Don't consider answers with a length that is either < 0 or > max_answer_length.
                    if end_index < start_index or end_index - start_index + 1 > max_answer_length:
                        continue
                    # Don't consider answer that don't have the maximum context available (if such information is
                    # provided).
                    if token_is_max_context is not None and not token_is_max_context.get(str(start_index), False):
                        continue
                    prelim_predictions.append(
                        {
                            "offsets": (offset_mapping[start_index][0], offset_mapping[end_index][1]),
                            "score": start_logits[start_index] + end_logits[end_index],
                            "start_logit": start_logits[start_index],
                            "end_logit": end_logits[end_index],
                        }
                    )
        if version_2_with_negative:
            # Add the minimum null prediction
            prelim_predictions.append(min_null_prediction)
            null_score = min_null_prediction["score"]

        # Only keep the best `n_best_size` predictions.
        predictions = sorted(prelim_predictions, key=lambda x: x["score"], reverse=True)[:n_best_size]

        # Add back the minimum null prediction if it was removed because of its low score.
        if version_2_with_negative and not any(p["offsets"] == (0, 0) for p in predictions):
            predictions.append(min_null_prediction)

        # Use the offsets to gather the answer text in the original context.
        context = example["context"]
        for pred in predictions:
            offsets = pred.pop("offsets")
            pred["text"] = context[offsets[0]: offsets[1]]

        # In the very rare edge case we have not a single non-null prediction, we create a fake prediction to avoid
        # failure.
        if len(predictions) == 0 or (len(predictions) == 1 and predictions[0]["text"] == ""):
            predictions.insert(0, {"text": "empty", "start_logit": 0.0, "end_logit": 0.0, "score": 0.0})

        # Compute the softmax of all scores (we do it with numpy to stay independent from torch/tf in this file, using
        # the LogSumExp trick).
        scores = np.array([pred.pop("score") for pred in predictions])
        exp_scores = np.exp(scores - np.max(scores))
        probs = exp_scores / exp_scores.sum()

        # Include the probabilities in our predictions.
        for prob, pred in zip(probs, predictions):
            pred["probability"] = prob

        # Pick the best prediction. If the null answer is not possible, this is easy.
        if not version_2_with_negative:
            all_predictions[example["id"]] = predictions[0]["text"]
        else:
            # Otherwise we first need to find the best non-empty prediction.
            i = 0
            while predictions[i]["text"] == "":
                i += 1
            best_non_null_pred = predictions[i]

            # Then we compare to the null prediction using the threshold.
            score_diff = null_score - best_non_null_pred["start_logit"] - best_non_null_pred["end_logit"]
            scores_diff_json[example["id"]] = float(score_diff)  # To be JSON-serializable.
            if score_diff > null_score_diff_threshold:
                all_predictions[example["id"]] = ""
            else:
                all_predictions[example["id"]] = best_non_null_pred["text"]

        # Make `predictions` JSON-serializable by casting np.float back to float.
        all_nbest_json[example["id"]] = [
            {k: (float(v) if isinstance(v, (np.float16, np.float32, np.float64)) else v) for k, v in pred.items()}
            for pred in predictions
        ]

    return all_predictions, all_nbest_json


def get_record_parts(rid, db):
    """
    Get sentence representation form database
    :param rid: str, record id
    :param db: sqad_db object, connection to database
    :return: str, str, str
    """
    vocabulary, _, kb = db.get_dicts()
    record = get_record(db, rid, word_parts='w')

    question_content = ''
    for question_sent in record['question']:
        for token in question_sent['sent']:
            question_content += f'{token["word"]} '

    correct_answer_content = ''
    for answer_selection_sent in record['a_sel']:
        for token in answer_selection_sent['sent']:
            correct_answer_content += f'{token["word"]} '

    answer_extraction_content = ''
    for answer_extraction_sent in record['a_ext']:
        for token in answer_extraction_sent['sent']:
            answer_extraction_content += f'{token["word"]} '

    return question_content, correct_answer_content, answer_extraction_content, record['q_type'], record['a_type']


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Fine tunning of xml roberta')
    parser.add_argument('--db_path', type=str,
                        required=False,
                        help='Database path')
    parser.add_argument('--url', type=str,
                        required=False, default='',
                        help='Database URL')
    parser.add_argument('--port', type=int,
                        required=False, default=None,
                        help='Server port')
    parser.add_argument('--notes', type=str,
                        required=False, default='',
                        help='Notes')
    args = parser.parse_args()

    if not os.path.exists(conf.output_dir):
        os.makedirs(conf.output_dir)

    # =========================================
    # Logging
    logging.basicConfig(filename=f'{conf.output_dir}/training.log',
                        filemode='a',
                        format='%(message)s',
                        level=logging.INFO)

    # =========================================
    # DB access
    db = None
    if (args.url and args.port) or args.db_path:
        if args.url and args.port:
            db = SqadDb(url=args.url, port=args.port, read_only=True)
        elif args.db_path:
            db = SqadDb(file_name=args.db_path, read_only=True)
    else:
        sys.stderr.write('Please specify --db_path or (--port and --url)')
        sys.exit()

    # =========================================
    # Loading training data
    czech_train_path = f'{conf.train_data}/train'
    train_dataset = datasets.load_dataset(
        'json',
        data_files=czech_train_path,
        field='data',
        split="train"
    )

    czech_test_path = f'{conf.train_data}/test'
    test_dataset = datasets.load_dataset(
        'json',
        data_files=czech_test_path,
        field='data',
        split="train"
    )

    czech_eval_path = f'{conf.train_data}/eval'
    eval_dataset = datasets.load_dataset(
        'json',
        data_files=czech_eval_path,
        field='data',
        split="train"
    )

    czech_dataset = datasets.dataset_dict.DatasetDict()
    czech_dataset['train'] = train_dataset
    czech_dataset['test'] = test_dataset
    czech_dataset['eval'] = eval_dataset

    logging.info('czech_dataset')
    logging.info(czech_dataset)

    # =========================================

    tokenizer = AutoTokenizer.from_pretrained(conf.model_path)
    assert isinstance(tokenizer, PreTrainedTokenizerFast)
    pad_on_right = tokenizer.padding_side == "right"

    tokenized_czech = czech_dataset.map(
        prepare_train_features,
        fn_kwargs={"tokenizer": tokenizer, "pad_on_right": pad_on_right},
        batched=True,
        remove_columns=czech_dataset['train'].column_names
    )

    model = AutoModelForQuestionAnswering.from_pretrained(conf.model_path)

    # https://huggingface.co/docs/transformers/v4.21.1/en/main_classes/trainer#transformers.TrainingArguments
    train_args = TrainingArguments(
        output_dir=conf.output_dir,
        evaluation_strategy=conf.strategy,
        learning_rate=conf.learning_rate,
        warmup_ratio=conf.warmup_ratio,
        gradient_accumulation_steps=conf.gradient_accumulation_steps,
        per_device_train_batch_size=conf.batch_size,
        per_device_eval_batch_size=conf.batch_size,
        num_train_epochs=conf.num_train_epochs,
        weight_decay=conf.weight_decay,
        save_strategy=conf.strategy,
        save_total_limit=5,
        optim=conf.optimizer,
        load_best_model_at_end=conf.load_best_model_at_end,
        logging_strategy=conf.strategy
    )

    trainer = Trainer(
        model,
        train_args,
        train_dataset=tokenized_czech['train'],
        eval_dataset=tokenized_czech['eval'],
        data_collator=default_data_collator,
        tokenizer=tokenizer
    )

    trainer.train()


    validation_features = czech_dataset['test'].map(
        prepare_validation_features,
        fn_kwargs={"tokenizer": tokenizer, "pad_on_right": pad_on_right},
        batched=True,
        remove_columns=czech_dataset['test'].column_names
    )


    raw_predictions = trainer.predict(validation_features)
    validation_features.set_format(type=validation_features.format["type"],
                                   columns=list(validation_features.features.keys()))

    final_predictions, n_best_predictions = postprocess_qa_predictions(czech_dataset['test'], validation_features,
                                                                       raw_predictions.predictions)

    metric = datasets.load_metric("squad")
    # New predictions
    formatted_predictions = [{"id": k, "prediction_text": v} for k, v in final_predictions.items()]
    # Original answers
    references = [{"id": ex["id"], "answers": ex["answers"]} for ex in czech_dataset['test']]
    # Scores
    scores = metric.compute(predictions=formatted_predictions, references=references)

    logging.info(scores)

    match = 0
    partial_match = 0
    un_match = 0
    total_num = 0
    os.mkdir(f'{conf.output_dir}/test_results/')

    for rec_id, predicted_text in final_predictions.items():
        total_num += 1
        question, answer_sentence, orig_answer_extraction, q_type, a_type = get_record_parts(rec_id, db)
        with open(f'{conf.output_dir}/test_results/{rec_id}', 'w') as f:

            f.write(f'Record: {rec_id}\n')
            f.write(f'Question: {question}\n')
            f.write(f'Type: q:{q_type} a:{a_type}\n')
            f.write(f'Answer selection: {answer_sentence}\n')
            f.write(f'Org extraction: {orig_answer_extraction}\n')
            f.write(f'New extraction: {predicted_text}\n')

            lower_pred = predicted_text.lower()
            lower_orig = orig_answer_extraction.lower()
            if lower_orig in lower_pred:
                match += 1
                f.write(f'Match\n')
            elif len(set(lower_orig.split(' ')) & set(lower_pred.split(' '))) > 0:
                partial_match += 1
                f.write(f'Partial match\n')
            else:
                un_match += 1
                f.write(f'Unmatch\n')

            f.write(f'=========================={conf.n_best_size} best result===================================\n')

            for n_best in n_best_predictions[rec_id]:
                f.write('{:.5f}: {}\n'.format(n_best["probability"], n_best["text"]))

    with open(f'{conf.output_dir}/test_results/full_results.txt', 'w') as f:
        f.write('Final results:\n')
        f.write(f'Match: {match} | {(match*100)/total_num}%\n')
        f.write(f'Partial match: {partial_match} | {(partial_match*100)/total_num}%\n')
        f.write(f'Unmatch: {un_match} | {(un_match*100)/total_num}%\n')
        f.write(f'Squad scores: {scores}\n')

    scores["description"] = 'Used full dataset to test the basic trained model xlm-roberta-large-squad2'
    scores['length train'] = len(czech_dataset['train'])
    scores['length eval'] = len(czech_dataset['eval'])
    scores['length test'] = len(czech_dataset['test'])

    training_output = f"{conf.output_dir}/{conf.model_name}-finetuned-{conf.version}"

    # Save model
    trainer.save_model(training_output)

    # Save training parameters
    conf.notes = args.notes
    conf.scores = scores
    conf.save_config(f'{training_output}.json')


if __name__ == '__main__':
    main()
